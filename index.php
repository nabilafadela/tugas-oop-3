<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');


$sheep = new Animal("shaun");

echo "Name = ". $sheep->name . "<br>"; // "shaun"
echo "Legs = " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded = " . $sheep->cold_blooded . "<br> <br>"; // "no"

$frog = new Frog("Buduk");

echo "Name = ". $frog->name . "<br>"; 
echo "Legs = " . $frog->legs . "<br>";
echo "Cold Blooded = " . $frog->cold_blooded . "<br>"; 
echo "Jump = " . $frog->jump . "<br> <br>";

$sungokong = new Ape("kera sakti");

echo "Name = ". $sungokong->name . "<br>"; 
echo "Legs = " . $sungokong->legs . "<br>";
echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>"; 
echo "Yell = " . $sungokong->yell . "<br>";


?>